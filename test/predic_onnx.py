import onnxruntime as ort
import numpy as np

# ONNX Runtime 세션을 생성하고 모델을 로드합니다.
ort_session = ort.InferenceSession("model.onnx")

# 모델의 입력 이름을 가져옵니다. 첫 번째 입력 레이어의 이름을 사용합니다.
input_name = ort_session.get_inputs()[0].name

# 입력 데이터를 준비합니다. PyTorch 모델이 torch.tensor([1.0])을 입력으로 받았으므로,
# ONNX Runtime에 맞는 형태로 변환해야 합니다. 이 경우, Numpy 배열을 사용합니다.
x = np.array([1.0], dtype=np.float32)  # 모델이 예상하는 입력 형태에 맞게 조정

# 모델을 실행하고 결과를 받습니다.
pred_onnx = ort_session.run(None, {input_name: x})

# 결과 출력
print("Predicted by ONNX Runtime:", pred_onnx)
