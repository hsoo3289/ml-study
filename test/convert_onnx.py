import torch
import torch.onnx

model = torch.load('test/model.pt')
model.eval()

## Export the model to ONNX
torch.onnx.export(model, torch.tensor([1.0]), 'test/model.onnx')

