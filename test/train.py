import torch
import torch.nn as nn
import torch.optim as optim

# 데이터 정의
X = torch.tensor([1, 2, 3, 4, 5], dtype=torch.float32).view(-1, 1)  # 입력
Y = torch.tensor([2, 4, 6, 8, 10], dtype=torch.float32).view(-1, 1) # 정답 (2의 배수)

# 모델 정의 (간단한 선형 회귀)
model = nn.Linear(in_features=1, out_features=1)

# 손실 함수 및 최적화기 정의
criterion = nn.MSELoss() # Mean Squared Error Loss
optimizer = optim.SGD(model.parameters(), lr=0.01) # Stochastic Gradient Descent

# 훈련 시작
n_epochs = 1000  # 에포크 설정
for epoch in range(n_epochs):
    # 예측
    pred = model(X)

    # 손실 계산
    loss = criterion(pred, Y)

    # 역전파
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    # 로그 출력
    if (epoch+1) % 50 == 0:
        print(f'Epoch {epoch+1}/{n_epochs}, Loss: {loss.item()}')

# 테스트
test_input = torch.tensor([1, 2, 3, 4, 5], dtype=torch.float32).view(-1, 1)
predicted = model(test_input).detach()
print("Predicted (after training)", test_input.view(1, -1).numpy(), predicted.view(1, -1).numpy())

# 모델의 상태 사전 저장
torch.save(model, 'test/model.pt')

print("Model saved to model.pt")
