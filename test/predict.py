import torch

model = torch.load('test/model.pt')

model.eval()
output = model(torch.tensor([1.0]))
print(output)
