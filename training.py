# 필요한 PyTorch 라이브러리를 불러옵니다.
import torch
import torch.nn as nn
import torch.nn.functional as F


# 신경망 모델 정의
class SimpleNet(nn.Module):
    def __init__(self):
        super(SimpleNet, self).__init__()
        # 입력층, 은닉층, 출력층의 뉴런 수를 정의합니다.
        self.fc1 = nn.Linear(in_features=10, out_features=5)  # 예시: 입력층 10개, 은닉층 5개
        self.fc2 = nn.Linear(5, 2)  # 은닉층 5개, 출력층 2개

    def forward(self, x):
        x = F.relu(self.fc1(x))  # 첫 번째 층을 통과한 후 ReLU 활성화 함수 적용
        x = self.fc2(x)  # 두 번째 층을 통과
        return x


# 모델 객체를 생성합니다.
net = SimpleNet()

# 임의의 입력 데이터 생성 (배치 크기: 1, 입력 특성: 10)
input = torch.randn(1, 10)

# 모델을 통해 입력 데이터 전달
output = net(input)

# 결과 출력
print(output)
